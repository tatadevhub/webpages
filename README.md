
## Clone a repository
git clone https://tatadevhub@bitbucket.org/tatadevhub/webpages.git

## install http-server

npm install --global http-server


## redirect to webpages and run 

http-server

webpage will open in 

Main Webpage : http://127.0.0.1:8080/
Past Events: http://127.0.0.1:8080/pastEvents.html
Registration: http://127.0.0.1:8080/registrationConfirmed.html

